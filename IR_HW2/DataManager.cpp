/*
Thai Thien
1351040
IR-HW1
*/
#include "DataManager.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include "Gatherer.h"
#include <string>
#include <algorithm>

void DataManager::addWord(std::string word, int docID) {
	using namespace std;
	if (!isInVocabulary(word))
		return;
	Entry entryword(word,docID);// a new entry with 1 doc
	set<Entry>::iterator pos;
	pos = wordList.find(entryword);
	if (pos == wordList.end()) {
		wordList.insert(entryword);// insert new entry to set
	}
	else
	{
		// add docID to exist entry
		Entry e = *pos;
		wordList.erase(e);
		e.addDocID(docID);
		wordList.insert(e);
	}
}


bool DataManager::isInVocabulary(std::string word) {
	Word_Id w(word);
	if (vocabulary.find(word) == vocabulary.end())
		return false;
	else return true;
}




void DataManager::printInvertedIndex() {
	using namespace std;
	set<Entry>::iterator pos;
	for (pos = wordList.begin(); pos != wordList.end(); pos++) {
		Entry e = *pos;
		cout << e.toString() << "\n";
	}

}

DataManager::DataManager()
{
}


DataManager::~DataManager()
{
}

Entry DataManager::findWord(std::string word) {
	using namespace std;
	set<Entry>::iterator pos;
	Entry e(word, -1);
	pos = wordList.find(e);
	
	Entry r("", -1);
	if (pos != wordList.end()) {
		Entry r = *pos;
		return r;
	}
	return r;
}

Entry DataManager::intersection(Entry a, Entry b) {

	Node* cur_a = a.getHead();
	Node* cur_b = b.getHead();
	Entry result("", -1);// init an empty entry for holding the intersection
	while (cur_a != NULL && cur_b != NULL) {
		if (cur_a->ID == cur_b->ID)
		{
			result.addDocID(cur_a->ID);
			cur_a = cur_a->pNext;
			cur_b = cur_b->pNext;
			continue;
		}

		if (cur_a->ID > cur_b->ID) {
			// a > b => increase b
			cur_b = cur_b->pNext;
		}
		else
		{
			//b > a => increase a
			cur_a = cur_a->pNext;
		}	
	}
	return result;
}


Entry DataManager::searchQuery(std::string query) {
	
	using namespace std;
	istringstream iss(query);
	vector<string> word_tk;
	copy(istream_iterator<string>(iss),
		istream_iterator<string>(),
		back_inserter(word_tk));
	Entry result("",-1);
	if (word_tk.size() == 0)
		return result;
	
	
	Entry one = findWord(Gatherer::cleanString(word_tk[0]));
	
	for (int i = 1; i < word_tk.size(); i++) {
		Entry two = findWord(Gatherer::cleanString(word_tk[i]));
		one = intersection(one, two);
	}

	return one;
}




void DataManager::addVocabulary(std::string word) {
	if (vocabulary.size() < MAX_VOCABULARY) {
		if (word != "") {
			vocabulary.insert(word);
		}
	}
}

void DataManager::printVocabulary() {
	using namespace std;
	ofstream fout;
	fout.open("Vocabulary.txt");

	set<Word_Id>::iterator pos;
	for (pos = vocabulary.begin(); pos != vocabulary.end(); pos++) {
		fout << *pos << "\n";
	}
	fout.close();
}

void DataManager::buildTermTFIDF() {
	using namespace std;
	set<Entry>::iterator pos;
	for (pos = wordList.begin(); pos != wordList.end(); pos++) {
		Entry e = *pos;
		tfidfItem term(&e);
		termTfIdf.insert(term);
	}
}

double DataManager::tf(std::string t, int d) {
	using namespace std;
	tfidfItem tmp(t);
	set<tfidfItem>::iterator pos = termTfIdf.find(tmp);
	tmp = *pos;
	return  tmp.getTF(d);
}

double DataManager::idf(std::string t) {
	using namespace std;
	tfidfItem tmp(t);
	set<tfidfItem>::iterator pos = termTfIdf.find(t);
	tmp = *pos;
	return  tmp.getIDF();
}

void DataManager::indexVocabulary() {
	using namespace std;
	int i = 0;
	set<Word_Id>::iterator pos;
	for (pos = vocabulary.begin(); pos != vocabulary.end(); pos++) {
		pos->id = i;
		i++;
	}
}