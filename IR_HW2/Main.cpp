/*
	Thai Thien
	1351040
	IR-HW1
*/
#include <string>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <sstream>
#include "StopWord.h"
#include "Gatherer.h"
#include "Entry.h"
#include "DataManager.h"
#include <ctime>
#include "constant.h"
#include "SearchEngine.h"

void testEntry();
void testGatherer();
void testEntry();
void testDataManager();



//TODO: max 2gb ram 
int main() {
	std::ifstream fin;
	std::string str;
	std::ofstream fout;
	DataManager dm;
	StopWord stopword(&dm);

	// 216410
	clock_t begin = clock();
	
	stopword.readStopwordList();

	for (int i = 0; i < FILE_NUM; i++) {
		stopword.processRawFile(dm.file_list[i]);// Read text file
		stopword.outputFile();
	//	std::cout << i << "\n";
	}
	dm.indexVocabulary();
	dm.printVocabulary();

	std::cout << "meow \n";



	clock_t end1 = clock();
	std::cout << "Time done " << (end1 - begin) << "\n";
	Gatherer gather(&dm);
	std::cout << "Start to read from file \n";
	for (int i = 0; i < FILE_NUM; i++) {
		gather.readFile(i);// Read text file
	//	std::cout << i << "\n";
	}
	
	dm.buildTermTFIDF();
	clock_t end2 = clock();
	std::cout << "Time done " << (end2 - end1) << "\n";
	//std::cout << "Start to print into dictionary.txt \n";
	
	////a
	//dm.printDictionary();// print dictionary to text file
	


	////b
	std::string a_term;
	std::cout << "Building Inverted Index: list all documents that contain a given term (input a term) \n";
	//std::cin >> a_term;
	a_term = "Food";
	a_term = Gatherer::cleanString(a_term);
	
	Entry a_term_result = dm.findWord(a_term);
	if (a_term_result.getWord() != "") {
		std::cout << a_term_result.toString() << "\n";
	}
	else {
		std::cout << "Not found "<< a_term<<"\n";
	}
	//

	std::cout << "Build tf-idf vector for doc " << dm.file_list[0] << "\n";
	SearchEngine search(&dm);
	std::vector<double> doc1tfidf = search.calDocTfIDF(0);
	std::vector<double> querytfidf = search.calQueryTfIDF("Food is Big.");
	std::cout << "distance between doc 0 and Food is Big :" << search.calDistance(doc1tfidf, querytfidf) << "\n";

	clock_t end3 = clock();
	std::cout << "Time done " << (end3 - end2) << "\n";

	std::cout << "Search Food is Big \n";
	int docID = search.query_distance("Food is Big");
	std::cout << "Result " << docID << "\n";
	clock_t end4 = clock();
	std::cout << "Time done " << (end4 - end3) << "\n";
}


void testGatherer() {
	DataManager dm;
	Gatherer gather(&dm);
	gather.readFile(1);

}

void testEntry() {
	Entry entry("dog", 3);
	entry.addDocID(1);
	entry.addDocID(9);
	entry.addDocID(5);
	entry.addDocID(10);
	entry.addDocID(11);
	entry.addDocID(9);
	entry.addDocID(1);
	std::cout << entry.toString()<<"\n";
	std::cout << entry.searchDocID(3) << " " << entry.searchDocID(4)<<"\n";
}

void testDataManager() {
	DataManager dm;
	dm.addWord("breakthrough", 1);
	dm.addWord("drug", 1);
	dm.addWord("for", 1);
	dm.addWord("diabetes", 1);

	dm.addWord("new", 2);
	dm.addWord("diabetes", 2);
	dm.addWord("drug", 2);

	dm.addWord("new", 3);
	dm.addWord("approach", 3);
	dm.addWord("for", 3);
	dm.addWord("treatment", 3);
	dm.addWord("of", 3);
	dm.addWord("diabetes", 3);

	dm.addWord("new", 4);
	dm.addWord("hopes", 4);
	dm.addWord("for", 4);
	dm.addWord("diabetes", 4);
	dm.addWord("patients", 4);
	
	std::cout << "DICTIONARY " << "\n";

	std::cout << "Inverted Index" << "\n";
	dm.printInvertedIndex();
}