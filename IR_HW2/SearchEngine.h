#pragma once
#include <vector>
#include "DataManager.h"
class SearchEngine
{
private:
	DataManager* dm;
	double dotproduct(std::vector<double> v1, std::vector<double> v2);
	double callength(std::vector<double> v);

public:
	SearchEngine(DataManager* dm);
	~SearchEngine();
	std::vector<double> calDocTfIDF(int docID);
	std::vector<double> calQueryTfIDF(std::string str);
	double calAngles(std::vector<double> v1, std::vector<double> v2);
	double calDistance(std::vector<double> v1, std::vector<double> v2);
	int query_distance(std::string str);
};

