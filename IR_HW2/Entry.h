/*
Thai Thien
1351040
IR-HW1
*/
#pragma once
#include <string>
struct Node
{
	Node* pNext;
	int ID;
	int num; //counting the frequence of word appear  
	Node() {
		num = 1; 
	}
};


class Entry
{
private:
	std::string word;// a word
	mutable Node* head;// a linked list, each node is a docID of document contain that word
	mutable int length; // length of invested index (number of documents the word appear)
public:
	
	Entry(std::string word, int docID);
	~Entry();

	bool searchDocID(int docID);// search if docID is in the linked list
	int getLength();
	void addDocID(int docID);// add docID to the linked list in increasing order
	std::string getWord();//get word
	std::string toString();// get string consists of a word and inverted index 
	Node* getHead();
	bool operator <(const Entry& d) const;
	bool operator ==(const Entry& d) const;
};

