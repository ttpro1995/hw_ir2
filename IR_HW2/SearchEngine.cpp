#include "SearchEngine.h"
#include <string>
#include <sstream>
#include "Gatherer.h"

double SearchEngine::dotproduct(std::vector<double> v1, std::vector<double> v2)
{
	double sum = 0;
	for (int i = 0; i < v1.size(); i++) {
		sum += v1[i] * v2[i];
	}
	return sum;
}

double SearchEngine::callength(std::vector<double> v)
{
	double sum = 0;
	for (int i = 0; i < v.size(); i++) {
		sum += v[i] * v[i];
	}
	sum = sqrt(sum);
	return sum;
}



SearchEngine::SearchEngine(DataManager* dm)
{
	this->dm = dm;
}


SearchEngine::~SearchEngine()
{
}

std::vector<double> SearchEngine::calDocTfIDF(int docID) {
	using namespace std;
	set<Word_Id>::iterator pos;
	vector <double> doctfidf;
	for (pos = dm->vocabulary.begin(); pos != dm->vocabulary.end(); pos++) {
		Word_Id word_id = *pos;
		string word = word_id.word;
		double tf = dm->tf(word, docID);
		double idf = dm->idf(word);
		double w = tf*idf;
		doctfidf.push_back(w);
	}
	return doctfidf;
}


std::vector<double> SearchEngine::calQueryTfIDF(std::string str) {
	using namespace std;
	
	vector <double> querytfidf;
	vector <double> idfv;
	for (int i = 0; i < dm->vocabulary.size(); i++) {
		querytfidf.push_back(0);
	}
	set<Word_Id>::iterator pos2;
	for (pos2 = dm->vocabulary.begin(); pos2 != dm->vocabulary.end();pos2++) {
		idfv.push_back(dm->idf(pos2->word));
	}

	istringstream iss(str);
	string tmp;
	while (iss >> tmp) {
		tmp = Gatherer::cleanString(tmp);
		set<Word_Id>::iterator pos;
		pos = dm->vocabulary.find(tmp);
		if (pos == dm->vocabulary.end())
			continue;

		int index = pos->id;
		querytfidf[index] += 1;//let this be counter of word
	}

	for (int i = 0; i < dm->vocabulary.size(); i++) {
		if (querytfidf[i] > 0) {
			int freq = querytfidf[i];
			double tf = 1 + log(freq);
			double idf = idfv[i];
			double w = tf*idf;
			querytfidf[i] = w;
		}
	}

	return querytfidf;
}

double SearchEngine::calAngles(std::vector<double> v1, std::vector<double> v2)
{
	double top = this->dotproduct(v1, v2);
	double lengthV1 = callength(v1);
	double lengthV2 = callength(v2);
	double sum = top / (lengthV1*lengthV2);
	return sum;
}

double SearchEngine::calDistance(std::vector<double> v1, std::vector<double> v2)
{
	double sum = 0;
	for (int i = 0; i < v1.size(); i++) {
		int tmp = v1[i] - v2[i];
		tmp = tmp*tmp;
		sum += tmp;
	}
	sum = sqrt(sum);
	return sum;
}

int SearchEngine::query_distance(std::string str)
{
	using namespace std;
	double min_distance = 99999;
	int minDocID = -1;

	vector<double> querytfidf = calQueryTfIDF(str);
	for (int i = 0; i < FILE_NUM; i++) {
		vector<double> doctfidf = calDocTfIDF(i);
		double result = calDistance(querytfidf, doctfidf);
		if (result < min_distance) {
			min_distance = result;
			minDocID = i;
		}
	}
	return minDocID;
}
