#pragma once
#include <string>
#include <vector>
#include "Entry.h"


class tfidfItem
{

private:
	std::string word;
	mutable double idf;
	mutable double* tf;
	mutable Entry* entry;
public:
	tfidfItem(Entry* entry);
	tfidfItem(std::string tmp);
	bool operator<(const tfidfItem & d) const;
	bool operator==(const tfidfItem & d) const;
	void calIDF();
	void calTF();
	~tfidfItem();
	double getIDF();
	double getTF(int docID);
	
};

