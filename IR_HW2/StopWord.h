#pragma once
#include <list>
#include "Entry.h"
#include "DataManager.h"
#include <vector>
#include <set>
class StopWord
{
private: 
	std::set<std::string> stopword;
	std::string wordinfile;
	std::string fileName;
	DataManager *dm;
public:
	StopWord(DataManager *dm);

	void addStopWord(std::string word);
	void add(std::string word);
	bool isStopWord(std::string word);
	



	//file 

	void readStopwordList();
	void processRawFile(std::string fileName);
	void outputFile();
};

