#include "StopWord.h"
#include <fstream>
#include "Gatherer.h"
#include <sstream>
#include <string>
#include <streambuf>
#include <vector>

#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <set>
StopWord::StopWord(DataManager *dm)
{
	this->dm = dm;
	wordinfile = "";
}



void StopWord::processRawFile(std::string fileName) {
	using namespace std;
	std::ifstream fin;
	this->fileName = fileName;
	this->wordinfile = "";
	fin.open("docs/" + fileName);
	std::string str;
	vector<string> list_of_word{ istream_iterator<string>{fin},
		istream_iterator<string>{} };
	vector<string>::iterator pos;
	
	for (pos = list_of_word.begin(); pos != list_of_word.end(); pos++) {
		string str = *pos;
		str = Gatherer::cleanString(str);
		add(str);
	}
	fin.close();
}

void StopWord::add(std::string word) {
	if (!isStopWord(word)) {
		wordinfile +=word+" ";
		dm->addVocabulary(word);
	}
}

void StopWord::addStopWord(std::string word) {
	stopword.insert(word);
	
}

bool StopWord::isStopWord(std::string word) {
	std::transform(word.begin(), word.end(), word.begin(), ::tolower);
	if (stopword.find(word) == stopword.end())
		return false;
	else return true;
}



void StopWord::outputFile() {
	//std::cout << wordinfile << "\n";
	//dm->addFileContent(wordinfile);

	using namespace std;
	ofstream fout;
	fout.open("nostopword/" + fileName);
	fout << wordinfile;
	fout.close();
}

void StopWord::readStopwordList() {
	std::ifstream fin;
	fin.open("stopwords_en.txt");
	std::string str;
	while (fin >> str) {
		addStopWord(str);
	}
	fin.close();
}

