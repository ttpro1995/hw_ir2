#pragma once
#include <string>
class Word_Id {
public:
	std::string word;
	mutable int id;
	Word_Id(std::string str) {
		word = str;
		id = -1;
	}
	friend std::ostream &operator<<(std::ostream &output,
		const Word_Id &D)
	{
		output << D.word << " " << D.id;
		return output;
	}

	bool operator<(const Word_Id & d) const;
};
