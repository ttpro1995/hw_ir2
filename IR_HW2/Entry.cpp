/*
Thai Thien
1351040
IR-HW1
*/
#include "Entry.h"



Entry::Entry(std::string word, int docID)
{
	this->word = word;
	if (docID == -1) {
		this->head = NULL;
		this->length = 0;
	}
	else {
		this->length = 1;
		this->head = new Node();
		this->head->ID = docID;
		this->head->pNext = NULL;
	}
}

bool Entry::searchDocID(int docID) {
	Node * cur;
	cur = this->head;
	while (cur != NULL)
	{
		if (cur->ID == docID) {
			return true;
		}
		else {
			cur = cur->pNext;
		}
	}

	return false;
}

void Entry::addDocID(int docID) {
	Node * cur;
	Node * last = NULL;
	cur = this->head;

	if (this->head == NULL)
	{
		// when head is null
		this->head = new Node();
		this->head->ID = docID;
		this->head->pNext = NULL;
		length += 1;
		return;
	}

	while (cur != NULL)
	{
		if (cur->ID == docID) {
			cur->num += 1; // increasing the counting by 1;
			return;
		}

		if (cur->ID > docID) {
			// cur < docID, and last not null, insert into last
			if (last != NULL) {
				last->pNext = new Node();
				last->pNext->ID = docID;
				last->pNext->pNext = cur;
				length += 1;// increase length 
				return;

			}
			else
			{
				// insert at start of list
				head = new Node();
				head->ID = docID;
				head->pNext = cur;
				length += 1;// increase length 
				return;
			}
		}

		// move on
		last = cur;
		cur = cur->pNext;
	}

	// docID is largest, insert at the end of list
	last->pNext = new Node();
	last->pNext->ID = docID;
	last->pNext->pNext = cur;
	length += 1;// increase length 
}

std::string Entry::toString() {
	
	std::string str = "";
	str += this->word + " ";
	Node* cur = head;
	while (cur != NULL) {
		str += "__"+ std::to_string(cur->ID)+"-"+std::to_string(cur->num);//docID and frequence of word
		cur = cur->pNext;
	}
	return str;
}

std::string Entry::getWord() {
	return word;
}

Node* Entry::getHead() {
	return this->head;
}

Entry::~Entry()
{
}

bool Entry::operator<(const Entry& d) const {
	if (this->word < d.word)
		return true;
	else
		return false;
}

bool Entry::operator==(const Entry& d) const {
	if (this->word == d.word)
		return true;
	else
		return false;
}

int Entry::getLength() {
	return length;
}