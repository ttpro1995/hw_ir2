#include "tfidfItem.h"
#include <math.h>       /* log */
#include "constant.h"

tfidfItem::tfidfItem(Entry* entry)
{
	this->entry = entry;
	this->word = entry->getWord();
	tf = new double[FILE_NUM];
	calIDF();
	calTF();
}

tfidfItem::tfidfItem(std::string tmp)
{
	this->word = tmp;
	entry = NULL;
	tf = NULL;
}

bool tfidfItem::operator<(const tfidfItem& d) const {

	if (this->word < d.word)
		return true;
	else
		return false;
}

bool tfidfItem::operator==(const tfidfItem& d) const {
	if (this->word == d.word)
		return true;
	else
		return false;
}

tfidfItem::~tfidfItem()
{
}

void tfidfItem::calIDF() {
	
		idf = 1 + log(FILE_NUM / entry->getLength());

}

void tfidfItem::calTF() {
	tf = new double[FILE_NUM];
	Node* cur = entry->getHead();
	for (int i = 0; i < FILE_NUM; i++) {
		tf[i] = 0;
		if (cur != NULL) {
			if (cur->ID == i) {//docid match
				tf[i] = 1 + log(cur->num) ;
			}
			else {// the docid not match
				tf[i] = 0;
			}
			cur = cur->pNext;
		}
		else {// no more inverted index
			tf[i] = 0;
		}
	}
}

double tfidfItem::getTF(int docID) {
	return tf[docID];
}

double tfidfItem::getIDF() {
	return idf;
}